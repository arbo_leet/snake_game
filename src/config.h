#ifndef CONFIG_H
#define CONFIG_H

#define TILE_SIZE	25
#define FIELD_WIDTH	25
#define FIELD_HEIGHT	15

#define FPS		5

#endif /* CONFIG_H */
